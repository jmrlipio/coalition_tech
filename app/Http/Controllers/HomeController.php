<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Request;
use File;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
	private $fileName;

    public function __construct( )
    {
    	$this->fileName = 'datafile.json';
    }

	public function index()
    {
    	$data = array();
    	if (Storage::disk('local')->exists($this->fileName))
		{
			$data = json_decode(Storage::get($this->fileName), true);
			/*echo '<pre>';
			print_r($data);
			echo '</pre>';
			dd('s');*/
			if($data != null)
			{
		    	usort($data, array($this, "sort_by_date"));
	    	} 
	    }
        
        return view('index', compact('data'));
    }


    public function sort_by_date($a, $b) {
	   $a = strtotime($a['created_at']);
	   $b = strtotime($b['created_at']);
	   
	   return ($a < $b) ? -1 : 1;
	}


    public function storeItem()
    {
    	if(Request::ajax())
        {
        	$name = Request::input('name');
        	$price = Request::input('price');
        	$quantity = Request::input('quantity');
        	
        	$id = sha1(round(microtime(true) * 1000));

        	$arr_inputs = array(
		    	'id'		=> $id,
		    	'name' 		=> Request::input('name'),
		    	'quantity' 	=> Request::input('quantity'),
		    	'price' 	=> Request::input('price'),
		    	'created_at'=> date('Y-m-d H:i:s')
		    );

			if (Storage::disk('local')->exists($this->fileName))
			{	
			    $current_data = json_decode(Storage::get($this->fileName), true);
			    
			    try 
			    {
			    	if($current_data != null)
			    	{
			    		array_push( $current_data, $arr_inputs );
			    		//Storage::delete($this->fileName);
			    		Storage::disk('local')->put($this->fileName, json_encode($current_data));			    		
			    	}
			    	else
			    	{
			    		Storage::disk('local')->put($this->fileName, json_encode(array($arr_inputs)));					
						$current_data = json_decode(Storage::get($this->fileName), true);						
			    	}

			    	return response()->json([
		                'status' => 'success! new data added', 
		                'data' => $current_data
		            ]);
			    	
			    } 
			    catch (Exception $e) 
			    {
			    	return response()->json([
		                'status' => 'error', 
		                'data' => $e
		            ]);
			    }
			    
			}
			else
			{
				try 
				{
					Storage::disk('local')->put($this->fileName, json_encode(array($arr_inputs)));					
					$current_data = json_decode(Storage::get($this->fileName), true);

					return response()->json([
		                'status' => 'ok! new file created.', 
		                'data' => $current_data
		            ]);
				} 
				catch (Exception $e) 
				{
					return response()->json([
		                'status' => 'error', 
		                'data' => $e
		            ]);
				}
				
			}			
           
        }
    }

    public function editItem()
    {
    	if(Request::ajax())
        {
        	$id = Request::input('id');
        	$name = Request::input('name');
        	$price = Request::input('price');
        	$quantity = Request::input('quantity');

        	$current_data = json_decode(Storage::get($this->fileName), true);
        	$new_data = array();
			
			foreach ($current_data as $key => $row)
			{
			    if( $row['id'] == $id)
        		{
        			$row['name'] = $name;
        			$row['quantity'] = $quantity;
        			$row['price'] = $price;
        		}
        		$new_data[] = $row;
			}

			Storage::disk('local')->put($this->fileName, json_encode($new_data));
        			
			return response()->json([
                'status' => 'update successsful', 
                'data' => $new_data
            ]);

        }
    }
    
}
