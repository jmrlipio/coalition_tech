<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', array('as' => 'index', 'uses' => 'HomeController@index'));
Route::post('add-item', array('as' => 'item.add.ajax-call','uses' => 'HomeController@storeItem'));
Route::post('edit-item', array('as' => 'item.edit.ajax-call','uses' => 'HomeController@editItem'));
Route::post('remove-item', array('as' => 'item.remove.ajax-call','uses' => 'HomeController@removeItem'));