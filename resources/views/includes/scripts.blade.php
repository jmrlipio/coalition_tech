<script src="{{ URL::asset('js/jquery-1.11.0.min.js') }}"></script> 
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

<script>
    
    $('input[name=price], input[name=quantity],input[name=mdl_price], input[name=mdl_quantity]').keyup(function () {     
      if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
           this.value = this.value.replace(/[^0-9\.]/g, '');
        }
    });

    $(document).ready(function() {
        var data = '';
        var list = $("ul.list-group");

        $("#form-add").on("submit", function(e){
            e.preventDefault();

            var _token = $("input[name=token]").val();
            var name = $("input[name=product_name]").val();
            var quantity = $("input[name=quantity]").val();
            var price = $("input[name=price]").val();
            var _this = $(this);
            
            $.ajax({
                url: "{{ URL::route('item.add.ajax-call') }}",
                type: 'POST', 
                data: { name:name, quantity:quantity, price:price, _token:_token },                   
                dataType: 'JSON',
             
                success: function (response) 
                {    
                    data = $.parseJSON(JSON.stringify(response.data));

                    for (var x = 0; x < data.length; x++){
                        var item = data[x];
                        
                        if(x == data.length-1){

                            list.append('<li class="list-group-item" data-id='+item.id+'>'+'<strong>name:</strong><span class="item_name">'+item.name+'</span><span class="pull-right">'+item.created_at+'</span><br /><strong>price:</strong> <span class="item_price">'+item.price+'</span><br /><strong>quantity:</strong> <span class="item_quantity">'+item.quantity+'</span><br> <strong>Total Value:</strong> <span class="total_value">'+ item.price * item.quantity +'</span> <div class="pull-right"><a href="#" class="edit-item-product" data-id='+item.id+' data-toggle="modal" data-target="#edit-modal">Edit</a></div></li>').hide().fadeIn('slow'); 
                        }                        
                    }   
                    _this.trigger('reset');                 

                },
                error: function() {
                    console.log("There was an error. Try again please!");
                }
            });

        });

        $("#form-edit").on("submit", function(e){
            e.preventDefault();

            var _token = $("input[name=mdl_token]").val();
            var id = $("input[name=mdl_id]").val();
            var name = $("input[name=mdl_name]").val();
            var quantity = $("input[name=mdl_quantity]").val();
            var price = $("input[name=mdl_price]").val();
            var list = $("ul.list-group");
            var _id = "";  

            $.ajax({
                url: "{{ URL::route('item.edit.ajax-call') }}",
                type: 'POST', 
                data: { id:id, name:name, quantity:quantity, price:price, _token:_token },                   
                dataType: 'JSON',
             
                success: function (response) 
                {   
                    var data = $.parseJSON(JSON.stringify(response.data));
                    
                    for (var x = 0; x < data.length; x++){
                        var item = data[x]; 
                    
                        list.find('li').each(function() { 
                             _id = $(this).attr("data-id"); 
                            
                            if(item.id == _id){                                
                                $(this).find('span.item_name').html(item.name);
                                $(this).find('span.item_quantity').html(item.quantity);
                                $(this).find('span.item_price').html(item.price);
                                $(this).find('span.total_value').html(item.price * item.quantity);
                            }             
                        });
                        
                        $('#edit-modal').modal('hide');
                    }

                },
                error: function() {
                    console.log("There was an error. Try again please!");
                }
            });

        });

        $(document).on('click', 'a.edit-item-product', function(){

            var id = $(this).attr('data-id');
            var _id = '';

            list.find('li').each(function() {   
                
                if (id == $(this).attr("data-id")) {
                    $('input[name="mdl_id"]').val(id);
                    $('input[name=mdl_name]').val($(this).find('span.item_name').text());
                    $('input[name=mdl_quantity]').val($(this).find('span.item_quantity').text());
                    $('input[name=mdl_price]').val($(this).find('span.item_price').text());
                    $('span#item_name').text($(this).find('span.item_name').text());
              }
            });
           
        });
       
    });

</script>