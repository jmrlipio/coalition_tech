<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="ecommerce project">
<meta name="author" content="jmrlipio">
<meta name="keywords" content="">

<!-- <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'> -->
<!-- styles -->

<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">

<link rel="shortcut icon" href="favicon.png">

