<!DOCTYPE html>
<html lang="en">
   <head>
      @include('includes.head')
      <title>Exam</title>
   </head>
   
   <body>


      <div class="container">
     
         @yield('content')

      </div> 
     
      @include('includes.scripts')
      @yield('scripts')
      
   </body>
</html>