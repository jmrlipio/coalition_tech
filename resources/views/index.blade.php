@extends('layouts.default-wrapper')
@section('content')

    <div class="row">
        <br>
        <br>
        <br>
        <div class="col-md-6">
            {!! Form::open(array('id' => 'form-add')) !!}
                <fieldset>
                    <input type="hidden" name="token" value="{{{ csrf_token() }}}">
                    <div class="form-group">
                        <label for="product_name">Item Name</label>
                        <input class="form-control" placeholder="Name" name="product_name" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="quantity">Item Quantity</label>
                        <input class="form-control" placeholder="Quantity" name="quantity" required>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="quantity">Item Price</label>
                            <input class="form-control" placeholder="price" name="price" required>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-md btn-success pull-right">
                        <i class="fa fa-check fa-fw"></i>
                        Add
                    </button>

                </fieldset>
            {!! Form::close() !!}
        </div>
        
    </div>
    <div class="row">

        <div id="items" class="col-md-6">
            <br>
            <h4>Items</h4>
            <ul class="list-group">
                @if(count($data) != 0 )
                    @foreach($data as $row)
                        <li class="list-group-item" data-id="{{ $row['id'] }}">
                            <strong>Name:</strong><span class="item_name">{{ $row['name'] }}</span><span class="pull-right">{{ $row['created_at'] }}</span><br />
                            <strong>Price:</strong> <span class="item_price">{{ $row['price'] }}</span><br />
                            <strong>Quantity:</strong> <span class="item_quantity">{{ $row['quantity'] }}</span> <br>  
                            <strong>Total Value:</strong> <span class="total_value">{{ $row['price'] * $row['quantity'] }}</span> 
                          <div class="pull-right"><a href="#" class="edit-item-product" data-id='{{ $row['id'] }}' data-toggle="modal" data-target="#edit-modal">Edit</a></div>
                        </li>
                    @endforeach
                @endif

            </ul>
            
        </div>
    </div>

    <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
        <div class="modal-dialog modal-sm">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Update <span id="item_name"></span></h4>
                </div>
                
                {!! Form::open(array('id' => 'form-edit')) !!}
                    <div class="modal-body">
                        <input type="hidden" name="mdl_token" value="{{{ csrf_token() }}}">
                        <input type="hidden" name="mdl_id">
                        <div class="form-group">
                            <label for="mdl_name">Item Name</label>
                            <input type="text" name="mdl_name" class="form-control" id="name-modal" placeholder="name">
                        </div>

                        <div class="form-group">
                            <label for="mdl_price">Item Price</label>
                            <input type="text" name="mdl_price" class="form-control" id="price-modal" placeholder="price">
                        </div>

                        <div class="form-group">
                            <label for="mdl_quantity">Item Quantity</label>
                            <input type="text" name="mdl_quantity" class="form-control" id="quantity-modal" placeholder="quantity">
                        </div>
                    </div>
                       
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Update</button>
                    </div>
                    
                {!! Form::close() !!}
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
        <div class="modal-dialog modal-sm">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete</h4>
                </div>
               
                {!! Form::open(array('id' => 'form-delete')) !!}
                    <div class="modal-body">
                        <input type="hidden" name="mdl_token_delete" value="{{{ csrf_token() }}}">
                        <input type="hidden" name="mdl_id_delete">
                        <p>Are you sure you want to delete this data?</p>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Delete</button>
                    </div>
                
                {!! Form::close() !!}
                
            </div>
        </div>
    </div>

@stop